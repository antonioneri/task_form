<x-layout>

    <x-header>
       Login
    </x-header>

    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                

                <form class="shadow p-4 mt-4" action="{{route('login')}}" method="POST">
                    @csrf
                   
                    <div class=" mt-3">
                        <label for="email">Indirizzo Email:</label>
                        <input type="email" name="email" class="form-control mt-1 shadow" id="email" placeholder="Inserisci email" required>
                    </div>

                    <div class=" mt-3">
                        <label for="password">Password:</label>
                        <input type="password" name="password" id="password" class="form-control mt-1 shadow" placeholder="Inserisci password" required>
                    </div>
                    <button type="submit" class="btn btn-outline-dark mt-3">Entra</button>
                </form>
                
            </div>
        </div>
    </div>
   

</x-layout>
