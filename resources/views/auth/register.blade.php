<x-layout>

    <x-header>
        Registrati
    </x-header>

    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
              

            <form method="POST" action="{{route('register')}}" class="shadow p-4 mt-4">
                @csrf

                
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                             @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                             @endforeach
                        </ul>
                    </div>
                @endif

                <div class="form-group">
                    <label for="name">Nome Utente:</label>
                    <input type="text" name="name" id="name" class="form-control mt-1 shadow" placeholder="Inserisci name" required>
                </div>

                <div class="form-group mt-3">
                    <label for="email">Indirizzo Email:</label>
                    <input type="email" name="email" id="email" class="form-control mt-1 shadow" placeholder="Inserisci email" required>
                </div>

                <div class="form-group mt-3">
                    <label for="password">Password:</label>
                    <input type="password" name="password" id="password" class="form-control mt-1 shadow" placeholder="Inserisci password" required>
                </div>

                <div class="form-group mt-3">
                    <label for="password_confirmation">Conferma Password:</label>
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control mt-1 shadow" placeholder="Conferma password" required>
                </div>

                
                <select name="select" class="form-select mt-3" aria-label="Default select example">
                    <option value="">-- Seleziona il tipo di utente --</option>
                    <option value="user">User</option>
                    <option value="admin">Admin</option>
                  </select>
                    

                <button type="submit" class="btn btn-outline-dark mt-3">Invia</button>
            </form>
            </div>
        </div>
    </div>


</x-layout>
