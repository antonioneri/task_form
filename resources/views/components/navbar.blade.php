<nav class="navbar navbar-expand-lg bg-light">
    <div class="container-fluid">
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="{{route("homepage")}}">Home</a>
          </li>

                {{-- //? REVISORE --}}
                @if(Auth::check() && Auth::user()->is_revisor)
          <li class="nav-item">
            <a class="nav-link" href="{{route("admin.index")}}">Utenti</a>
          </li>
          @endif

              {{-- //? UTENTE NON REGISTRATO --}}
          @guest
          <li class="nav-item">
            <a class="nav-link" href="{{route("register")}}">Registrati</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route("login")}}">Accedi</a>
          </li>
          
          @else
              {{-- //? UTENTE REGISTRATO --}}
          <li class="nav-item">
            <a class="nav-link">Salve {{Auth::user()->name}}</a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link" href="/logout" onclick="event.preventDefault();getElementById('form-logout').submit();">Logout</a>
          </li>
          <form id="form-logout" action="{{route('logout')}}" method="POST" class="d-none">
          @csrf
        </form>
      </ul>
      @endguest
      
      </div>
    </div>
  </nav> 