<x-layout>


    <x-header>
        ricerca
    </x-header>


    <div class="container-fluid mt-5 text-center">

        <table class="table table-dark table-striped">
            <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                </tr>
            </thead>
            @forelse ($results as $result)
            <tr>
                <td>{{ $result->name }}</td>
                <td>{{ $result->email }}</td>
            </tr>
            @empty
                <div class="col-12">
                    <div class="alert alert-danger py-3 shadow text-center">
                        <p class="lead">La ricerca non ha prodotto nessun risultato</p>
                    </div>
                </div>
            @endforelse
        </table>

    </div>



</x-layout>