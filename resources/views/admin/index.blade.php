<x-layout>

    <x-header>
        Lista Iscritti
    </x-header>

    


    <form action="{{route('admin.search')}}" method="GET" class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="d-flex">
                    <input name="query" class="form-control" type="search" placeholder="Search any product...">
                    <button type="submit" class="ms-3 btn btn-outline-success">Search</button>    
                </div>
            </div>
        </div>
    </form>


    <div class="container-fluid mt-5 text-center">

        <table class="table table-dark table-striped">
            <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Revisore</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                    <tr>
                        <th>{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->is_revisor ? "si"  : "no"}}</td>
                    </tr>
                @endforeach

            </tbody>
        </table>

    </div>



</x-layout>
