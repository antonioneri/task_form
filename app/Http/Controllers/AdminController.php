<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
         $users = User::all();
 
         return view('admin.index', compact('users'));
    } 


    public function search(Request $request){

        $query = $request->input('query');
        $results = User::where('name', 'like', "%$query%")
            ->orWhere('email', 'like', "%$query%")
            ->get();

        return view('admin.search', compact('results'));


    }
}
